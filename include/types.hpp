#ifndef TYPES_HPP_INCLUDED
#define TYPES_HPP_INCLUDED

typedef struct Point
{
    int x;
    int y;

    Point():x(0), y(0)
    {}

    Point(int xValue, int yValue):x(xValue), y(yValue)
    {}
} tPoint;

#endif // TYPES_HPP_INCLUDED
