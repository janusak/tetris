#ifndef FIELD_HPP_INCLUDED
#define FIELD_HPP_INCLUDED

#include <vector>
#include "types.hpp"

class tField
{
public:
    tField(const tPoint& size, const tPoint& center);
    tField(const std::vector<std::vector<int> > & tilesValue, const tPoint& centerValue);
    bool isFilled(const tPoint& point) const;
    bool isPartOfField(const tPoint& point) const;
    int getTile(const tPoint& point) const;
    bool setTile(const tPoint& point, const int& value);
    tPoint getCenter() const;
    tPoint getLeftUpPoint() const;
    tPoint getWidthAndHeight() const;
    std::vector<std::vector<int> > getTiles() const;
protected:
    tPoint center;
    std::vector<std::vector<int> > tiles;
private:
    tPoint getVectorIndexes(const tPoint& point) const;
};

#endif // FIELD_HPP_INCLUDED
