#ifndef FIELDRICH_HPP_INCLUDED
#define FIELDRICH_HPP_INCLUDED

#include "field.hpp"
#include "figPatterns.hpp"

class tFieldTetris:public tField
{
public:
    tFieldTetris(const tPoint& size);
    bool update();
private:
    bool isLineFilled(const int& line);
    void init(const tPoint& size);
};

#endif // FIELDRICH_HPP_INCLUDED
