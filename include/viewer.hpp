#ifndef VIEWER_HPP_INCLUDED
#define VIEWER_HPP_INCLUDED

#include "drawdata.hpp"

void drawTiles(const tDrawData& drawData);

#endif // VIEWER_HPP_INCLUDED
