#ifndef FUNCTIONS_HPP_INCLUDED
#define FUNCTIONS_HPP_INCLUDED

#include <vector>
#include "types.hpp"

template <typename T>
tPoint getMatrixMaxSize(std::vector<std::vector<T> > matrix)
{
    tPoint answer;
    answer.y = matrix.size();
    int maxX = 0;
    for(int i = 0; i < answer.y; i++)
    {
        int size = matrix[i].size();
        if(size > maxX) maxX = size;
    }
    answer.x = maxX;
    return answer;
}

#endif // FUNCTIONS_HPP_INCLUDED
