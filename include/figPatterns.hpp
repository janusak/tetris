#ifndef FIGPATERNS_HPP_INCLUDED
#define FIGPATERNS_HPP_INCLUDED

#include <vector>
#include "types.hpp"

class tFigurePattern
{
public:
    tFigurePattern(unsigned int figureNumber);
    static unsigned int getMaxFigureNumber();
    static tPoint getMaxFigSize();
    void nextFigState();
    void prevFigState();
    std::vector<std::vector<int> > getFigState();
// TODO Сделать тесты для getFigureSize
    tPoint getFigureSize();
private:
    unsigned int state = 0;
    unsigned int number = 0;
    unsigned int getMaxFigState();
};

#endif // FIGPATERNS_HPP_INCLUDED
