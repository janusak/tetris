#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include <random>
#include "figure.hpp"
#include "fieldTetris.hpp"
#include "drawdata.hpp"

class tGame
{
public:
    tGame(const tPoint& size, int ticksPerTurnValue = 100);
    void setKeysGetter(int (*callback)(void));
    void setDrawer(void (*callback)(const tDrawData&));
    void setTickPauseFuction(void (*callback)(void));
    void startLoop();

private:
    int ticksPerTurn;
    tFieldTetris field;
    std::default_random_engine generator;
    std::uniform_int_distribution < int > distribution;
    tFigure figure;
    bool loopOn = true;
    int gameTick = 0;
    int (*getKeys)();
    void (*tickPause)(void);
    void (*sendToDraw)(const tDrawData& drawField);

    void newGameTurn();
    void KeyBoardHandler();
// TODO Сделать тесты для игровой логики
    void loop();
// TODO Сделать тесты для getRandomFigureNumber
    int getRandomFigureNumber();
// TODO Сделать тесты для getRandomFigure
    tFigure getRandomFigure();
    void sendDrawData();
    void gameLogic();
    void moveDown();
};

#endif // GAME_HPP_INCLUDED
