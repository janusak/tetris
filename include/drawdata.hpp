#ifndef DRAWDATA_HPP_INCLUDED
#define DRAWDATA_HPP_INCLUDED

#include <vector>

typedef struct
{
    std::vector<std::vector<int> > tiles;
}tDrawData;

#endif // DRAWDATA_HPP_INCLUDED
