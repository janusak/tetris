#ifndef SYSTEM_H_INCLUDED
#define SYSTEM_H_INCLUDED

#include <ncurses.h>
#include "system.hpp"

int init();
void stop();
void tickTimeout();
int getKeys();

#endif // SYSTEM_H_INCLUDED
