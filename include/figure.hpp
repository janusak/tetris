#ifndef FIGURE_HPP_INCLUDED
#define FIGURE_HPP_INCLUDED

#include <random>
#include "figPatterns.hpp"
#include "field.hpp"

class tFigure
{
public:
    tFigure(unsigned int figNum);
    static unsigned int getMaxFigureNumber();
    bool moveLeft(const tField& fieldToMerge);
    bool moveRight(const tField& fieldToMerge);
    bool turn(const tField& fieldToMerge);
    bool drop(const tField& fieldToMerge);
    bool moveDown(const tField& fieldToMerge);
    void merge(tField& fieldToMerge);
    tPoint getFigureSize();
// TODO написать тесты на getColor
    int getColor();
private:
    tFigurePattern figure;
    tField field;
    tPoint position;

    bool tryMerge(const tField& fieldToMerge);
    void updateField();
    int color;
};

#endif // FIGURE_HPP_INCLUDED
