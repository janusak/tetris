#include "UnitTest++.h"

#include "../include/functions.hpp"

namespace
{
SUITE(FunctionsTest)
{
    TEST(getMatrixMaxSize_full)
    {
        std::vector<std::vector<int> > matrix =
        {
            {0, 0, 0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0},
            {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, },
            {},
            {0, 0, 0, 0, 0, 0}
        };
        tPoint check = getMatrixMaxSize(matrix);

        CHECK_EQUAL(10, check.x);
        CHECK_EQUAL(5, check.y);
    }

    TEST(getMatrixMaxSize_empty)
    {
        std::vector<std::vector<int> > matrix;
        tPoint check = getMatrixMaxSize(matrix);

        CHECK_EQUAL(0, check.x);
        CHECK_EQUAL(0, check.y);
    }

    TEST(getMatrixMaxSize_halfempty)
    {
        std::vector<std::vector<int> > matrix = std::vector<std::vector<int> > (10);
        tPoint check = getMatrixMaxSize(matrix);

        CHECK_EQUAL(0, check.x);
        CHECK_EQUAL(10, check.y);
    }
}
}
