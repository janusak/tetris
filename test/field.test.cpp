#include "UnitTest++.h"
#include <vector>
#include <exception>
#include <cstdint>

#define private public
#define protected public

#include "../include/field.hpp"

namespace
{

SUITE(tFieldTest)
{
    struct FixtureDate
    {
        const tPoint size = {5, 6};
        const tPoint center = {2, 3};
        const tPoint leftUp = {-center.x, -center.y};
        const tPoint rightDown = {leftUp.x + size.x - 1, leftUp.x + size.x - 1};
        const tPoint leftUpInternal = {leftUp.x + 1, leftUp.y + 1};
        const tPoint rightDownInternal = {rightDown.x - 1, rightDown.y - 1};
        const tPoint leftUpExternal = {leftUp.x - 1, leftUp.y - 1};
        const tPoint rightDownExternal = {rightDown.x + 1, rightDown.y + 1};
        const std::vector<std::vector<int> > tilesFull =
        {
            {11, 12, 13, 14, 15},
            {21, 22, 23, 24, 25},
            {31, 32, 33, 34, 35},
            {41, 42, 43, 44, 45},
            {51, 52, 53, 54, 55},
            {61, 62, 63, 64, 65},
        };
        const std::vector<std::vector<int> > tilesEmpty =
        {
            { 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0},
            { 0, 0, 0, 0, 0}
        };

        FixtureDate() {}
    };

    struct FixtureTilesFull:FixtureDate
    {
        int centerValue = tilesFull[center.y - 1][center.x - 1];
        int leftUpValue = tilesFull[0][0];
        int rightDownValue = tilesFull[size.y - 1][size.x - 1];
        int leftUpInternalValue = leftUpValue + 11;
        int rightDownInternalValue = rightDownValue - 11;
        int leftUpExternalValue = 0;
        int rightDownExternalValue = 0;
        tField field = tField(tilesFull, center);

        FixtureTilesFull():FixtureDate() {}
    };

    struct FixtureTilesEmpty:FixtureDate
    {
        tField field = tField(tilesEmpty, center);

        FixtureTilesEmpty():FixtureDate() {}
    };

    struct FixtureAsimetryTiles
    {
        const tPoint size = {5, 6};
        const tPoint center = {2, 3};
        std::vector<std::vector<int> > tilesAsimetry =
        {
            {11, 12, 13, },
            {21, 22, 23, 24, 25},
            {31, 32, 33},
            {41},
            {},
            {61, 62, 63, 64}
        };
        std::vector<std::vector<int> > tilesAsimetryExp =
        {
            {11, 12, 13, 00, 00},
            {21, 22, 23, 24, 25},
            {31, 32, 33, 00, 00},
            {41, 00, 00, 00, 00},
            {00, 00, 00, 00, 00},
            {61, 62, 63, 64, 00}
        };

        FixtureAsimetryTiles() {}
    };

    TEST_FIXTURE(FixtureDate, Constructor_Point)
    {
        (void )tField(size, center);
    }

    TEST_FIXTURE(FixtureDate, Constructor_Tiles)
    {
        (void )tField(tilesFull, center);
    }

    TEST_FIXTURE(FixtureTilesEmpty, Constructor_Point_Check)
    {
        tField checkedField = tField(size, center);

        CHECK_ARRAY2D_CLOSE(checkedField.tiles, tilesEmpty, size.y, size.x, 0.1);
        CHECK_EQUAL(center.x, checkedField.center.x);
        CHECK_EQUAL(center.x, checkedField.center.x);
    }

    TEST_FIXTURE(FixtureTilesFull, Constructor_Tiles_Check)
    {
        tField checkedField = tField(tilesFull, center);

        CHECK_ARRAY2D_CLOSE(checkedField.tiles, tilesFull, size.y, size.x, 0.1);
        CHECK_EQUAL(center.x, checkedField.center.x);
        CHECK_EQUAL(center.x, checkedField.center.x);
    }

    TEST(Constructor_WrongPoint)
    {
        tPoint size1 = {0, 0};
        tPoint size2 = {1, 0};
        tPoint size3 = {0, 1};
        tPoint size4 = {-1, 1};
        tPoint size5 = {1, -1};
        tPoint center = {2, 1};

        CHECK_THROW((void)tField(size1, center), std::exception);
        CHECK_THROW((void)tField(size2, center), std::exception);
        CHECK_THROW((void)tField(size3, center), std::exception);
        CHECK_THROW((void)tField(size4, center), std::exception);
        CHECK_THROW((void)tField(size5, center), std::exception);
    }

    TEST_FIXTURE(FixtureAsimetryTiles, Constructor_TilesAsimetry)
    {
        (void)tField(tilesAsimetry, center);
    }

    TEST_FIXTURE(FixtureAsimetryTiles, Constructor_TilesAsimetry_Check)
    {
        tField field = tField(tilesAsimetry, center);

        CHECK_ARRAY2D_CLOSE(tilesAsimetryExp, field.tiles, size.y, size.x, 0);
        CHECK_EQUAL(center.x, field.center.x);
        CHECK_EQUAL(center.y, field.center.y);
    }

    TEST_FIXTURE(FixtureTilesFull, getLeftUpPoint)
    {
        tPoint point = field.getLeftUpPoint();

        CHECK_EQUAL(-center.x, point.x);
        CHECK_EQUAL(-center.y, point.y);
    }

    TEST_FIXTURE(FixtureTilesFull, getWightAndHeight)
    {
        tPoint point = field.getLeftUpPoint();

        CHECK_EQUAL(-center.x, point.x);
        CHECK_EQUAL(-center.y, point.y);
    }

    TEST_FIXTURE(FixtureTilesFull, getCenter)
    {
        tPoint point = field.getCenter();

        CHECK_EQUAL(center.x, point.x);
        CHECK_EQUAL(center.y, point.y);
    }

/// Для не чистого поля все точки входящие в границы поля возвращают истину
    TEST_FIXTURE(FixtureTilesFull, isPartOfField_leftUp)
    {
        CHECK(field.isPartOfField(leftUp));
    }

    TEST_FIXTURE(FixtureTilesFull, isPartOfField_rightDown)
    {
        CHECK(field.isPartOfField(rightDown));
    }

    TEST_FIXTURE(FixtureTilesFull, isPartOfField_leftUpInternal)
    {
        CHECK(field.isPartOfField(rightDownInternal));
    }

    TEST_FIXTURE(FixtureTilesFull, isPartOfField_rightDownInternal)
    {
        CHECK(field.isPartOfField(leftUpInternal));
    }

    TEST_FIXTURE(FixtureTilesFull, isPartOfField_rightDownExternal)
    {
        CHECK(!field.isPartOfField(leftUpExternal));
    }

    TEST_FIXTURE(FixtureTilesFull, isPartOfField_leftUpExternal)
    {
        CHECK(!field.isPartOfField(rightDownExternal));
    }

/// Для не чистого поля все точки входящие в границы поля возвращают истину
    TEST_FIXTURE(FixtureTilesFull, isFilled_leftUp)
    {
        CHECK(field.isFilled(leftUp));
    }

    TEST_FIXTURE(FixtureTilesFull, isFilled_rightDown)
    {
        CHECK(field.isFilled(rightDown));
    }

    TEST_FIXTURE(FixtureTilesFull, isFilled_leftUpInternal)
    {
        CHECK(field.isFilled(rightDownInternal));
    }

    TEST_FIXTURE(FixtureTilesFull, isFilled_rightDownInternal)
    {
        CHECK(field.isFilled(leftUpInternal));
    }

    TEST_FIXTURE(FixtureTilesFull, isFilled_rightDownExternal)
    {
        CHECK(field.isFilled(leftUpExternal));
    }

    TEST_FIXTURE(FixtureTilesFull, isFilled_leftUpExternal)
    {
        CHECK(field.isFilled(rightDownExternal));
    }

/// Для чистого поля все точки входящие в границы поля и выходящие за неё возвращают ложь
    TEST_FIXTURE(FixtureTilesEmpty, isFilled_leftUp)
    {
        CHECK(!field.isFilled(leftUp));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isFilled_rightDown)
    {
        CHECK(!field.isFilled(rightDown));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isFilled_leftUpInternal)
    {
        CHECK(!field.isFilled(rightDownInternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isFilled_rightDownInternal)
    {
        CHECK(!field.isFilled(leftUpInternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isFilled_rightDownExternal)
    {
        CHECK(field.isFilled(leftUpExternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isFilled_leftUpExternal)
    {
        CHECK(field.isFilled(rightDownExternal));
    }

/// Для чистого поля все точки входящие в границы поля возвращают истину
    TEST_FIXTURE(FixtureTilesEmpty, isPartOfField_leftUp)
    {
        CHECK(field.isPartOfField(leftUp));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isPartOfField_rightDown)
    {
        CHECK(field.isPartOfField(rightDown));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isPartOfField_leftUpInternal)
    {
        CHECK(field.isPartOfField(rightDownInternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isPartOfField_rightDownInternal)
    {
        CHECK(field.isPartOfField(leftUpInternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isPartOfField_rightDownExternal)
    {
        CHECK(!field.isPartOfField(leftUpExternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, isPartOfField_leftUpExternal)
    {
        CHECK(!field.isPartOfField(rightDownExternal));
    }

/// Для не чистого поля все точки входящие в границы поля значение вектора, а выходящие за неё 0
    TEST_FIXTURE(FixtureTilesFull, getTile_leftUp)
    {
        CHECK_EQUAL(leftUpValue, field.getTile(leftUp));
    }

    TEST_FIXTURE(FixtureTilesFull, getTile_rightDown)
    {
        CHECK_EQUAL(rightDownValue, field.getTile(rightDown));
    }

    TEST_FIXTURE(FixtureTilesFull, getTile_leftUpInternal)
    {
        CHECK_EQUAL(rightDownInternalValue, field.getTile(rightDownInternal));
    }

    TEST_FIXTURE(FixtureTilesFull, getTile_rightDownInternal)
    {
        CHECK_EQUAL(leftUpInternalValue, field.getTile(leftUpInternal));
    }

    TEST_FIXTURE(FixtureTilesFull, getTile_rightDownExternal)
    {
        CHECK_EQUAL(leftUpExternalValue, field.getTile(leftUpExternal));
    }

    TEST_FIXTURE(FixtureTilesFull, getTile_leftUpExternal)
    {
        CHECK_EQUAL(rightDownExternalValue, field.getTile(rightDownExternal));
    }

/// Для чистого поля все точки входящие в границы поля возвращают истину
    TEST_FIXTURE(FixtureTilesEmpty, setTile_leftUp)
    {
        CHECK(field.setTile(leftUp, 10));
        CHECK_EQUAL(10, field.getTile(leftUp));
    }

    TEST_FIXTURE(FixtureTilesEmpty, setTile_rightDown)
    {
        CHECK(field.setTile(rightDown, 10));
        CHECK_EQUAL(10, field.getTile(rightDown));
    }

    TEST_FIXTURE(FixtureTilesEmpty, setTile_leftUpInternal)
    {
        CHECK(field.setTile(leftUpInternal, 10));
        CHECK_EQUAL(10, field.getTile(leftUpInternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, setTile_rightDownInternal)
    {
        CHECK(field.setTile(rightDownInternal, 10));
        CHECK_EQUAL(10, field.getTile(rightDownInternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, setTile_leftUpExternal)
    {
        CHECK(!field.setTile(leftUpExternal, 10));
        CHECK_EQUAL(0, field.getTile(leftUpExternal));
    }

    TEST_FIXTURE(FixtureTilesEmpty, setTile_rightDownExternal)
    {
        CHECK(!field.setTile(rightDownExternal, 10));
        CHECK_EQUAL(0, field.getTile(rightDownExternal));
    }

    TEST_FIXTURE(FixtureTilesFull, getTiles)
    {
        CHECK_ARRAY2D_CLOSE(tilesFull, field.getTiles(), tilesFull.size(), tilesFull[0].size(), 0);
    }
}
}


