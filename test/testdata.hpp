#ifndef TESTDATA_HPP_INCLUDED
#define TESTDATA_HPP_INCLUDED

#include <vector>

namespace testData
{
extern std::vector < std::vector<std::vector<int> > > figureBeamPositions;
}
#endif // TESTDATA_HPP_INCLUDED
