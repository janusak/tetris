#include "UnitTest++.h"
#include <vector>
#include <exception>
#include <cstdint>

#define private public
#define protected public
#include "../include/figPatterns.hpp"
#undef private
#undef protected

#include "testdata.hpp"

#include "../include/functions.hpp"
namespace
{
SUITE(tFigPatternsTest)
{
    struct FixtureTestFig
    {
        tFigurePattern figure = tFigurePattern(tFigurePattern::getMaxFigureNumber());
        FixtureTestFig() {}
    };

    TEST(getMaxFigureNumber)
    {
        CHECK_EQUAL((unsigned int)7, tFigurePattern::getMaxFigureNumber());
    }

    TEST(Constructor_TrowThenWrongValue)
    {
        CHECK_THROW(tFigurePattern(tFigurePattern::getMaxFigureNumber() + 1), std::exception);
        CHECK_THROW(tFigurePattern(0), std::exception);
    }

    TEST(Constructor_Normal)
    {
        tFigurePattern(tFigurePattern::getMaxFigureNumber());
    }

    TEST(Constructor_Normal1)
    {
        tFigurePattern figure = tFigurePattern(tFigurePattern::getMaxFigureNumber());

        CHECK_EQUAL((unsigned int)0, figure.state);
        CHECK_EQUAL(tFigurePattern::getMaxFigureNumber() - 1, figure.number);
    }

    TEST_FIXTURE(FixtureTestFig, MeamSwitch)
    {
        figure.state = 2;
        std::vector<std::vector<int> > testVec = figure.getFigState();

        CHECK_ARRAY2D_CLOSE(testData::figureBeamPositions[2], testVec, testData::figureBeamPositions[2].size(), testData::figureBeamPositions[2][0].size(), 0.1);
    }

    TEST_FIXTURE(FixtureTestFig, MeamSwitchForward)
    {
        figure.state = 2;
        figure.nextFigState();
        std::vector<std::vector<int> > testVec = figure.getFigState();

        CHECK_ARRAY2D_CLOSE(testData::figureBeamPositions[3], testVec, testData::figureBeamPositions[3].size(), testData::figureBeamPositions[3][0].size(), 0.1);
    }

    TEST_FIXTURE(FixtureTestFig, MeamSwitchBackward)
    {
        figure.state = 2;
        figure.prevFigState();
        std::vector<std::vector<int> > testVec = figure.getFigState();

        CHECK_ARRAY2D_CLOSE(testData::figureBeamPositions[1], testVec, testData::figureBeamPositions[1].size(), testData::figureBeamPositions[1][0].size(), 0.1);
    }

    TEST_FIXTURE(FixtureTestFig, UpSwitchBackward)
    {
        figure.state = 0;
        std::vector<std::vector<int> > testVec = figure.getFigState();

        CHECK_ARRAY2D_CLOSE(testData::figureBeamPositions[0], testVec, testData::figureBeamPositions[0].size(), testData::figureBeamPositions[0][0].size(), 0.1);

        figure.prevFigState();

        CHECK_EQUAL((unsigned int)3, figure.state);

        testVec = figure.getFigState();

        CHECK_ARRAY2D_CLOSE(testData::figureBeamPositions[3], testVec, testData::figureBeamPositions[3].size(), testData::figureBeamPositions[3][0].size(), 0.1);
    }

    TEST_FIXTURE(FixtureTestFig, DownSwitchForward)
    {
        figure.state = 3;
        std::vector<std::vector<int> > testVec = figure.getFigState();

        CHECK_ARRAY2D_CLOSE(testData::figureBeamPositions[3], testVec, testData::figureBeamPositions[3].size(), testData::figureBeamPositions[3][0].size(), 0.1);

        figure.nextFigState();

        CHECK_EQUAL((unsigned int)0, figure.state);

        testVec = figure.getFigState();

        CHECK_ARRAY2D_CLOSE(testData::figureBeamPositions[0], testVec, testData::figureBeamPositions[0].size(), testData::figureBeamPositions[0][0].size(), 0.1);
    }

    TEST_FIXTURE(FixtureTestFig, getFigureSize)
    {
        std::vector<std::vector<int> > tiles = figure.getFigState();
        tPoint check = figure.getFigureSize();
        tPoint exp = getMatrixMaxSize(tiles);

        CHECK_EQUAL(exp.x, check.x);
        CHECK_EQUAL(exp.y, check.y);
    }
}
}
