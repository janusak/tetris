#include "UnitTest++.h"

#include "../include/field.hpp"
#define private public
#define protected public
#include "figure.hpp"
#undef private
#undef protected
#include "testdata.hpp"

namespace
{
SUITE(tFigureTest)
{
    struct FixtureFig
    {
        std::vector<std::vector<int> > getTiles(tPoint& sizeValue)
        {
            std::vector<std::vector<int> > answer = std::vector<std::vector<int> > (sizeValue.y);
            for(unsigned int i = 0; i < (unsigned int)sizeValue.y; i++)
            {
                answer[i] = std::vector <int> (sizeValue.x);
            }
            return answer;
        }
        tPoint size = {6, 6};
        std::vector<std::vector<int> > tiles = getTiles(size);
        tPoint center = {size.x / 2, tFigurePattern::getMaxFigSize().y / 2};
        tFigure figure = tFigure(tFigure::getMaxFigureNumber());
        int col = tFigure::getMaxFigureNumber();
    };

    struct FixtureFigMotion:FixtureFig
    {
        tField field = tField(size, center);
        FixtureFigMotion():FixtureFig() {}
    };

    TEST(Constructor_getMaxFigureNumber)
    {
        CHECK_EQUAL(tFigurePattern::getMaxFigureNumber(), tFigure::getMaxFigureNumber());
    }

    TEST(Constructor_Default)
    {
        tFigure figure = tFigure(tFigure::getMaxFigureNumber());

        CHECK_EQUAL(tFigure::getMaxFigureNumber(), (unsigned int)figure.color);
        CHECK_EQUAL(0, figure.position.x);
        CHECK_EQUAL(0, figure.position.y);
    }

    TEST(Constructor_Normal)
    {
        tFigure figure = tFigure(tFigure::getMaxFigureNumber());

        CHECK_EQUAL(tFigure::getMaxFigureNumber(), (unsigned int)figure.color);
        CHECK_EQUAL(0, figure.position.x);
        CHECK_EQUAL(0, figure.position.y);
    }

    TEST(Constructor_WrongValue)
    {
        CHECK_THROW(tFigure(tFigure::getMaxFigureNumber() + 1), std::exception);
    }
    TEST_FIXTURE(FixtureFigMotion, mergeOk1)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };

        CHECK_EQUAL(size.y, (int)expTiles.size());
        CHECK_EQUAL(size.x, (int)expTiles[0].size());

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFig, MoveOk2)
    {
        std::vector<std::vector<int> > vectFilled =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', 000, 000, 000, '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        tField checkField = tField(vectFilled, center);
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', col, col, col, '#'},
            {'#', '#', '#', col, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };

        CHECK_EQUAL(size.y, (int)expTiles.size());
        CHECK_EQUAL(size.x, (int)expTiles[0].size());

        figure.merge(checkField);
        vectFilled = checkField.getTiles();

        CHECK_ARRAY2D_CLOSE(expTiles, vectFilled, size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFig, MergeException1)
    {
        tiles[1][2] = '#';
        tField fieldCheck = tField(tiles, center);

        CHECK_THROW(figure.merge(fieldCheck), std::exception);
    }

    TEST_FIXTURE(FixtureFigMotion, MergeException2)
    {
        tiles[1][3] = '#';
        tField fieldCheck = tField(tiles, center);

        CHECK_THROW(figure.merge(fieldCheck), std::exception);
    }

    TEST_FIXTURE(FixtureFigMotion, MergeException3)
    {
        tiles[1][4] = '#';
        tField fieldCheck = tField(tiles, center);

        CHECK_THROW(figure.merge(fieldCheck), std::exception);
    }

    TEST_FIXTURE(FixtureFigMotion, MergeException4)
    {
        tiles[2][3] = '#';
        tField fieldCheck = tField(tiles, center);

        CHECK_THROW(figure.merge(fieldCheck), std::exception);
    }

    TEST_FIXTURE(FixtureFigMotion, MoveRightOk0)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, col, col, col},
            {000, 000, 000, 000, col, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };

        CHECK(figure.moveRight(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, MoveRightFail1)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, '#'},
            {000, 000, 000, col, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[1][5] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.moveRight(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), expTiles.size(), expTiles[0].size(), 0.1);
    }

    TEST_FIXTURE(FixtureFigMotion, MoveRightFail2)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, '#', 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[2][4] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.moveRight(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), expTiles.size(), expTiles[0].size(), 0.1);
    }

    TEST_FIXTURE(FixtureFig, MoveRightOk)
    {
        std::vector<std::vector<int> > vectFilled =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', 000, 000, 000},
            {'#', '#', '#', '#', 000, '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        tField checkField = tField(vectFilled, center);
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', col, col, col},
            {'#', '#', '#', '#', col, '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };

        CHECK(figure.moveRight(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, MoveLeft1)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, '#', col, col, col, 000},
            {000, 000, 000, col, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[1][1] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.moveLeft(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, MoveLeft2)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, '#', col, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[2][2] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.moveLeft(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }
    TEST_FIXTURE(FixtureFig, MoveLeftOk)
    {
        std::vector<std::vector<int> > vectFilled =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', 000, 000, 000, '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        tField checkField = tField(vectFilled, center);
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', col, col, col, '#', '#'},
            {'#', '#', col, '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };

        CHECK(figure.moveLeft(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, TurnFail)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, '#', 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[0][3] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.turn(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFig, TurnOk1)
    {
        std::vector<std::vector<int> > vectFilled =
        {
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', '#', 000, 000, '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        tField checkField = tField(vectFilled, center);
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', '#', col, '#', '#'},
            {'#', '#', '#', col, col, '#'},
            {'#', '#', '#', col, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };

        CHECK(figure.turn(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, turnOk2)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, '#', col, '#', 000},
            {000, 000, col, col, 000, 000},
            {000, 000, '#', col, '#', 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[0][2] = '#';
        tiles[0][4] = '#';
        tiles[2][2] = '#';
        tiles[2][4] = '#';
        tField checkField = tField(tiles, center);
        figure.turn(checkField);
        figure.turn(checkField);
        figure.turn(checkField);
        figure.merge(checkField);
        figure.turn(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, turnOk3)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, col},
            {000, 000, 000, 000, col, col},
            {000, 000, 000, 000, 000, col}
        };

        CHECK(figure.moveRight(field));
        CHECK(figure.turn(field));
        CHECK(figure.turn(field));
        CHECK(figure.turn(field));
        CHECK(figure.moveRight(field));
        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(!figure.moveDown(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, moveDownFail1)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, '#', col, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[2][2] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.moveDown(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, moveDownFail2)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, 000, 000},
            {000, 000, 000, '#', 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[3][3] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.moveDown(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, moveDownFail3)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, '#', 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };
        tiles[2][4] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.moveDown(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFig, moveDownOk1)
    {
        std::vector<std::vector<int> > vectFilled =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', 000, 000, 000, '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        tField checkField = tField(vectFilled, center);
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', col, col, col, '#'},
            {'#', '#', '#', col, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };

        CHECK(figure.moveDown(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, moveDowmOk2)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, 000, 000}
        };

        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(!figure.moveDown(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, moveDowmOk3)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, col, col, col},
            {000, 000, 000, 000, col, 000}
        };

        CHECK(figure.moveRight(field));
        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(!figure.moveDown(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, moveDowmOk4)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {col, col, col, 000, 000, 000},
            {000, col, 000, 000, 000, 000}
        };

        CHECK(figure.moveLeft(field));
        CHECK(figure.moveLeft(field));
        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(figure.moveDown(field));
        CHECK(!figure.moveDown(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFigMotion, DropFail)
    {

        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, 000, 000},
            {000, 000, 000, '#', 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000}
        };

        tiles[3][3] = '#';
        tField checkField = tField(tiles, center);

        CHECK(!figure.drop(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }

    TEST_FIXTURE(FixtureFig, dropOk1)
    {
        std::vector<std::vector<int> > vectFilled =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', 000, 000, 000, '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        tField checkField = tField(vectFilled, center);
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', col, col, col, '#'},
            {'#', '#', '#', col, '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };

        CHECK(figure.drop(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }
    TEST_FIXTURE(FixtureFig, dropOk2)
    {
        std::vector<std::vector<int> > vectFilled =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', 000, 000, 000, '#'},
            {'#', '#', 000, 000, 000, '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        tField checkField = tField(vectFilled, center);
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', 000, 000, 000, '#'},
            {'#', '#', col, col, col, '#'},
            {'#', '#', '#', col, '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };

        CHECK(figure.drop(checkField));

        figure.merge(checkField);

        CHECK_ARRAY2D_CLOSE(expTiles, checkField.getTiles(), size.x, size.y, 0);
    }
    TEST_FIXTURE(FixtureFigMotion, dropOk3)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000},
            {000, 000, 000, col, 000, 000}
        };

        CHECK(figure.drop(field));
        CHECK(!figure.drop(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }
    TEST_FIXTURE(FixtureFigMotion, dropOk4)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, col, col, col},
            {000, 000, 000, 000, col, 000}
        };

        CHECK(figure.moveRight(field));
        CHECK(figure.drop(field));
        CHECK(!figure.drop(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }
    TEST_FIXTURE(FixtureFigMotion, dropOk5)
    {
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {col, col, col, 000, 000, 000},
            {000, col, 000, 000, 000, 000}
        };

        CHECK(figure.moveLeft(field));
        CHECK(figure.moveLeft(field));
        CHECK(figure.drop(field));
        CHECK(!figure.drop(field));

        figure.merge(field);

        CHECK_ARRAY2D_CLOSE(expTiles, field.getTiles(), size.x, size.y, 0);
    }
}
}
