#include "UnitTest++.h"

#define private public
#define protected public
#include "../include/fieldTetris.hpp"
#undef private
#undef protected

namespace
{
SUITE(tFieldTetrisTest)
{
    TEST(Constructor_NotThrow)
    {
        tPoint fieldSize = tFigurePattern::getMaxFigSize();
        (void)tFieldTetris(fieldSize);
    }

    TEST(Constructor_Throw1)
    {
        tPoint fieldSize = tFigurePattern::getMaxFigSize();
        fieldSize.y--;

        CHECK_THROW((void)tFieldTetris(fieldSize), std::exception);
    }

    TEST(Constructor_Throw2)
    {
        tPoint fieldSize = tFigurePattern::getMaxFigSize();
        fieldSize.x--;

        CHECK_THROW((void)tFieldTetris(fieldSize), std::exception);
    }

    TEST(Constructor_Throw3)
    {
        tPoint fieldSize = {-100, 100};

        CHECK_THROW((void)tFieldTetris(fieldSize), std::exception);
    }

    TEST(Constructor_Throw4)
    {
        tPoint fieldSize = {100, -100};

        CHECK_THROW((void)tFieldTetris(fieldSize), std::exception);
    }

    TEST(Constructor2)
    {
        tPoint size = {299, 600};
        tFieldTetris fieldTetris = tFieldTetris(size);
        tPoint widthAndHeight = fieldTetris.getWidthAndHeight();

        CHECK_EQUAL(299, widthAndHeight.x);
        CHECK_EQUAL(600, widthAndHeight.y);

        tPoint leftUp = fieldTetris.getLeftUpPoint();

        CHECK_EQUAL(-149, leftUp.x);
        CHECK_EQUAL(-tFigurePattern::getMaxFigSize().y / 2, leftUp.y);
    }

    TEST(update1)
    {
        std::vector<std::vector<int> > tiles =
        {
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'}
        };
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', 000, '#', '#', '#'}
        };
        tPoint size = {(int)tiles[0].size(), (int)tiles.size()};
        tFieldTetris fieldTetris = tFieldTetris(size);
        fieldTetris.tiles = tiles;

        CHECK(fieldTetris.update());
        CHECK(!fieldTetris.update());
        CHECK_ARRAY2D_CLOSE(expTiles, fieldTetris.tiles, expTiles.size(), expTiles[0].size(), 0.1);
    }

    TEST(update2)
    {
        std::vector<std::vector<int> > tiles =
        {
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', '#', '#', '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'}
        };
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'}
        };
        tPoint size = {(int)tiles[0].size(), (int)tiles.size()};
        tFieldTetris fieldTetris = tFieldTetris(size);
        fieldTetris.tiles = tiles;

        CHECK(fieldTetris.update());
        CHECK(!fieldTetris.update());
        CHECK_ARRAY2D_CLOSE(expTiles, fieldTetris.tiles, expTiles.size(), expTiles[0].size(), 0.1);
    }

    TEST(update3)
    {
        std::vector<std::vector<int> > tiles =
        {
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'}
        };
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'}
        };
        tPoint size = {(int)tiles[0].size(), (int)tiles.size()};
        tFieldTetris fieldTetris = tFieldTetris(size);
        fieldTetris.tiles = tiles;

        CHECK(!fieldTetris.update());
        CHECK(!fieldTetris.update());
        CHECK_ARRAY2D_CLOSE(expTiles, fieldTetris.tiles, expTiles.size(), expTiles[0].size(), 0.1);
    }

    TEST(update4)
    {
        std::vector<std::vector<int> > tiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, '#', 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {'#', '#', '#', '#', '#', '#'},
            {000, 000, 000, '#', 000, 000},
            {000, '#', 000, 000, 000, 000}
        };
        std::vector<std::vector<int> > expTiles =
        {
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, '#', 000, 000, 000},
            {000, 000, 000, 000, 000, 000},
            {000, 000, 000, '#', 000, 000},
            {000, '#', 000, 000, 000, 000}
        };
        tPoint size = {(int)tiles[0].size(), (int)tiles.size()};
        tFieldTetris fieldTetris = tFieldTetris(size);
        fieldTetris.tiles = tiles;

        CHECK(fieldTetris.update());
        CHECK(!fieldTetris.update());
        CHECK_ARRAY2D_CLOSE(expTiles, fieldTetris.tiles, expTiles.size(), expTiles[0].size(), 0.1);
    }

    TEST(getField2)
    {
        std::vector<std::vector<int> > tiles =
        {
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'}
        };
        std::vector<std::vector<int> > expTiles =
        {
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', '#', '#', 000},
            {000, '#', '#', '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'},
            {'#', '#', 000, '#', '#', '#'},
            {'#', '#', '#', 000, '#', '#'}
        };
        tPoint size = {(int)tiles[0].size(), (int)tiles.size()};
        tFieldTetris field = tFieldTetris(size);
        field.tiles = tiles;

        CHECK_ARRAY2D_CLOSE(expTiles, field.tiles, expTiles.size(), expTiles[0].size(), 0.1);
    }
}
}
