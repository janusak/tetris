#include "UnitTest++.h"

#define private public
#define protected public
#include "game.hpp"
#undef private
#undef protected
#include <ncurses.h>

namespace
{
SUITE(tGameTest)
{
    const tPoint size = {8, 6};

    struct FixtureFunctions
    {
        static unsigned int IterationNumber;
        FixtureFunctions()
        {
            IterationNumber = 0;
        }
    };

    TEST(Constructor2)
    {
        tPoint point = {125, 12};
        (void)tGame(point);
    }

    TEST(Constructor_Throw)
    {
        tPoint point = {-125, 12};

        CHECK_THROW((void)tGame(point), std::exception);
    }

    TEST(Constructor_default1)
    {
        tGame game = tGame(size);
        game.sendToDraw(tDrawData());
    }

    TEST(Constructor_default2)
    {
        tGame game = tGame(size);
        game.getKeys();
    }

    TEST(Constructor_default3)
    {
        tGame game = tGame(size);
        game.tickPause();
    }

    TEST(loop)
    {
        tGame game = tGame(size);
        game.loop();
    }

    TEST(newGameTurn)
    {
        tGame game = tGame(size);
        game.gameTick = 100;

        CHECK_EQUAL(100, game.gameTick);

        game.newGameTurn();

        CHECK_EQUAL(0, game.gameTick);
    }

    namespace callbackTest
    {
    tDrawData arrayForDraw;
    int keyPressNothingMockCounter = 0;
    int keyPressUpMockCounter = 0;
    int keyPressDownMockCounter = 0;
    int keyPressRightMockCounter = 0;
    int keyPressLeftMockCounter = 0;
    int timeSleepMockCounter = 0;
    int drawMockCounter = 0;

    int keyPressNothingMock()
    {
        keyPressNothingMockCounter++;
        return 0;
    }

    int keyPressUpMock()
    {
        keyPressUpMockCounter++;
        return KEY_UP;
    }

    int keyPressDownMock()
    {
        keyPressDownMockCounter++;
        return KEY_DOWN;
    }

    int keyPressRightMock()
    {
        keyPressRightMockCounter++;
        return KEY_RIGHT;
    }

    int keyPressLeftMock()
    {
        keyPressLeftMockCounter++;
        return KEY_LEFT;
    }

    void timeSleepMock()
    {
        timeSleepMockCounter++;
    }

    void drawMock(const tDrawData& draw)
    {
        arrayForDraw = draw;
        drawMockCounter++;
    }
    }

    struct FixtureCallback
    {
        tGame game;

        FixtureCallback():game(size)
        {
            callbackTest::keyPressNothingMockCounter = 0;
            callbackTest::keyPressUpMockCounter = 0;
            callbackTest::keyPressDownMockCounter = 0;
            callbackTest::keyPressRightMockCounter = 0;
            callbackTest::keyPressLeftMockCounter = 0;
            callbackTest::timeSleepMockCounter = 0;
            callbackTest::drawMockCounter = 0;
            callbackTest::arrayForDraw =
            {
                std::vector<std::vector<int> > ()
            };
        }
    };

    TEST_FIXTURE(FixtureCallback, setKeysGetter)
    {
        game.setKeysGetter(callbackTest::keyPressNothingMock);

        CHECK_EQUAL(callbackTest::keyPressNothingMock, game.getKeys);
    }

    TEST_FIXTURE(FixtureCallback, setTickPauseFuction)
    {
        game.setTickPauseFuction(callbackTest::timeSleepMock);

        CHECK_EQUAL(callbackTest::timeSleepMock, game.tickPause);

        game.tickPause();

        CHECK_EQUAL(1, callbackTest::timeSleepMockCounter);
    }

    TEST_FIXTURE(FixtureCallback, setDrawer)
    {
        game.setDrawer(callbackTest::drawMock);

        CHECK_EQUAL(callbackTest::drawMock, game.sendToDraw);
    }

    struct FixtureGameTest:FixtureCallback
    {
        FixtureGameTest():FixtureCallback()
        {
            game.setDrawer(callbackTest::drawMock);
            game.setTickPauseFuction(callbackTest::timeSleepMock);
            game.figure = tFigure(tFigurePattern::getMaxFigureNumber());
        }
    };

    TEST_FIXTURE(FixtureGameTest, FixtureGameTest)
    {
        CHECK_EQUAL(callbackTest::drawMock, game.sendToDraw);
        CHECK_EQUAL(callbackTest::timeSleepMock, game.tickPause);
    }

    TEST_FIXTURE(FixtureGameTest, sendDrawData)
    {
        int col = game.figure.getColor();
        std::vector<std::vector<int> > expectedTiles =
        {
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, col, col, col, 000, 000},
            {000, 000, 000, 000, col, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
        };
        game.sendDrawData();

        CHECK_EQUAL(expectedTiles.size(), callbackTest::arrayForDraw.tiles.size());
        CHECK_EQUAL(expectedTiles[0].size(), callbackTest::arrayForDraw.tiles[0].size());
        CHECK_ARRAY2D_CLOSE(expectedTiles, callbackTest::arrayForDraw.tiles, expectedTiles.size(), expectedTiles[0].size(), 0);
    }

    TEST_FIXTURE(FixtureGameTest, pushKeyUp)
    {
        int col = game.figure.getColor();
        std::vector<std::vector<int> > expectedTiles =
        {
            {000, 000, 000, 000, col, 000, 000, 000},
            {000, 000, 000, 000, col, col, 000, 000},
            {000, 000, 000, 000, col, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
        };
        game.setKeysGetter(callbackTest::keyPressUpMock);
        game.loop();

        CHECK_EQUAL(1, callbackTest::keyPressUpMockCounter);
        CHECK_EQUAL(expectedTiles.size(), callbackTest::arrayForDraw.tiles.size());
        CHECK_EQUAL(expectedTiles[0].size(), callbackTest::arrayForDraw.tiles[0].size());
        CHECK_ARRAY2D_CLOSE(expectedTiles, callbackTest::arrayForDraw.tiles, expectedTiles.size(), expectedTiles[0].size(), 0);
    }

    TEST_FIXTURE(FixtureGameTest, pushKeyDown)
    {
        int col = game.figure.getColor();
        std::vector<std::vector<int> > expectedTiles =
        {
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, col, col, col, 000, 000},
            {000, 000, 000, 000, col, 000, 000, 000},
        };
        game.setKeysGetter(callbackTest::keyPressDownMock);
        game.loop();

        CHECK_EQUAL(1, callbackTest::keyPressDownMockCounter);
        CHECK_EQUAL(expectedTiles.size(), callbackTest::arrayForDraw.tiles.size());
        CHECK_EQUAL(expectedTiles[0].size(), callbackTest::arrayForDraw.tiles[0].size());
        CHECK_ARRAY2D_CLOSE(expectedTiles, callbackTest::arrayForDraw.tiles, expectedTiles.size(), expectedTiles[0].size(), 0);
    }

    TEST_FIXTURE(FixtureGameTest, pushKeyRight)
    {
        int col = game.figure.getColor();
        std::vector<std::vector<int> > expectedTiles =
        {
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, col, col, col, 000},
            {000, 000, 000, 000, 000, col, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
        };
        game.setKeysGetter(callbackTest::keyPressRightMock);
        game.loop();

        CHECK_EQUAL(1, callbackTest::keyPressRightMockCounter);
        CHECK_EQUAL(expectedTiles.size(), callbackTest::arrayForDraw.tiles.size());
        CHECK_EQUAL(expectedTiles[0].size(), callbackTest::arrayForDraw.tiles[0].size());
        CHECK_ARRAY2D_CLOSE(expectedTiles, callbackTest::arrayForDraw.tiles, expectedTiles.size(), expectedTiles[0].size(), 0);
    }

    TEST_FIXTURE(FixtureGameTest, pushKeyLeft)
    {
        int col = game.figure.getColor();
        std::vector<std::vector<int> > expectedTiles =
        {
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, col, col, col, 000, 000, 000},
            {000, 000, 000, col, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
            {000, 000, 000, 000, 000, 000, 000, 000},
        };
        game.setKeysGetter(callbackTest::keyPressLeftMock);
        game.loop();

        CHECK_EQUAL(1, callbackTest::keyPressLeftMockCounter);
        CHECK_EQUAL(expectedTiles.size(), callbackTest::arrayForDraw.tiles.size());
        CHECK_EQUAL(expectedTiles[0].size(), callbackTest::arrayForDraw.tiles[0].size());
        CHECK_ARRAY2D_CLOSE(expectedTiles, callbackTest::arrayForDraw.tiles, expectedTiles.size(), expectedTiles[0].size(), 0);
    }

    namespace gameLogic
    {

    }

    struct FixtureGameLogicTest
    {
        tGame game;

        FixtureGameLogicTest():game(size)
        {
            game.setDrawer(callbackTest::drawMock);
            game.setTickPauseFuction(callbackTest::timeSleepMock);
            game.figure = tFigure(tFigurePattern::getMaxFigureNumber());
        }
    };
}
}
