#include "game.hpp"
#include "field.hpp"
#include "fieldTetris.hpp"
#include "viewer.hpp"
#include "ncurses.h"

static int getKeysDoNothing();
static void tickPauseDoNothing();
static void drawDoNothing(const tDrawData&);

static int getKeysDoNothing()
{
    return 0;
}

static void tickPauseDoNothing()
{

}

static void drawDoNothing(const tDrawData& unused)
{
    (void)unused;
}

tGame::tGame(const tPoint& size, int ticksPerTurnValue):
    ticksPerTurn(ticksPerTurnValue),
    field(size),
    generator(0),
    distribution(1, tFigurePattern::getMaxFigureNumber()),
    figure(getRandomFigureNumber())
{
    setKeysGetter(getKeysDoNothing);
    setDrawer(drawDoNothing);
    setTickPauseFuction(tickPauseDoNothing);
}

void tGame::setKeysGetter(int ( * callback)(void))
{
    getKeys = callback;
}

void tGame::setDrawer(void ( * callback)(const tDrawData&))
{
    sendToDraw = callback;
}

void tGame::setTickPauseFuction(void ( * callback)(void))
{
    tickPause = callback;
}

void tGame::loop()
{
    KeyBoardHandler();
    sendDrawData();
    tickPause();
    gameLogic();
}

void tGame::startLoop()
{
    while(1)
    {
        loop();
    }
}

void tGame::newGameTurn()
{
    gameTick = 0;
}

int tGame::getRandomFigureNumber()
{
    return distribution(generator);
}

tFigure tGame::getRandomFigure()
{
    return tFigure(getRandomFigureNumber());
}

void tGame::KeyBoardHandler()
{
    int key = getKeys();
    switch(key)
    {
    case KEY_UP:
        figure.turn(field);
        break;
    case KEY_RIGHT:
        figure.moveRight(field);
        break;
    case KEY_LEFT:
        figure.moveLeft(field);
        break;
    case KEY_DOWN:
        if(figure.drop(field))
        {
            newGameTurn();
        }
    default:
        break;
    }
}

void tGame::sendDrawData()
{
    tFieldTetris fieldForDraw = field;
    figure.merge(fieldForDraw);
    tDrawData drawData =
    {
        fieldForDraw.getTiles()
    };
    sendToDraw(drawData);
}

void tGame::gameLogic()
{
    if(gameTick % ticksPerTurn == 0)
    {
        moveDown();
    }
    gameTick++;
}

void tGame::moveDown()
{
    if(!figure.moveDown(field))
    {
        figure.merge(field);
        field.update();
        figure = getRandomFigure();
    }
}
