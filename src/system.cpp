#include <unistd.h>
#include <ncurses.h>
#include <sys/ioctl.h>
#include "system.hpp"

static bool kbhit();

int init()
{
    static int isInit = 0;
    if(!isInit)
    {
        if (!initscr())
        {
            fprintf(stderr, "Error initialising ncurses.\n");
            return 1;
        }
        curs_set(0);
        refresh();
        noecho();
        cbreak();
        raw();
        keypad(stdscr, TRUE);
        isInit = 1;
    }
    return 0;
}

void stop()
{
    endwin();
}

void tickTimeout()
{
    usleep(10000);
}

bool kbhit()
{
    init();
    int left = 0;
    ioctl(0, FIONREAD, &left);
    if(left != 0) return true;
    return false;
}

int getKeys()
{
    int key = 0;
    if(kbhit())
    {
        key = getch();
    }
    return key;
}
