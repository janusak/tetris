#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "system.hpp"
#include "field.hpp"
#include "viewer.hpp"
#include "game.hpp"
#include "time.h"
#include <unistd.h>

int main()
{
    init();
    move(0, 0);
    printw("Welcome to TETRIS!");
    refresh();

    sleep(2);
    tPoint size = {10, 21};
    tGame game = tGame(size);
    game.setKeysGetter(getKeys);
    game.setDrawer(drawTiles);
    game.setTickPauseFuction(tickTimeout);
    game.startLoop();
    sleep(6);
    stop();
}
