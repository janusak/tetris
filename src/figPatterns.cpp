#include "figPatterns.hpp"
#include "functions.hpp"

static std::vector < std::vector < std::vector<std::vector<int> > >> figures =
{
    {
        {
            {000, 000, 000, 000},
            {000, '#', '#', 000},
            {000, '#', '#', 000},
            {000, 000, 000, 000},
        }
    }, {
        {
            {000, 000, 000, 000},
            {'#', '#', '#', '#'},
            {000, 000, 000, 000},
            {000, 000, 000, 000},
        },
        {
            {000, 000, '#', 000},
            {000, 000, '#', 000},
            {000, 000, '#', 000},
            {000, 000, '#', 000},
        },
    }, {
        {
            {000, 000, '#', '#'},
            {000, '#', '#', 000},
            {000, 000, 000, 000},
            {000, 000, 000, 000},
        },
        {
            {000, 000, '#', 000},
            {000, 000, '#', '#'},
            {000, 000, 000, '#'},
            {000, 000, 000, 000},
        },
    }, {
        {
            {000, '#', '#', 000},
            {000, 000, '#', '#'},
            {000, 000, 000, 000},
            {000, 000, 000, 000},
        },
        {
            {000, 000, 000, '#'},
            {000, 000, '#', '#'},
            {000, 000, '#', 000},
            {000, 000, 000, 000},
        },
    }, {
        {
            {000, 000, 000, 000},
            {000, '#', '#', '#'},
            {000, 000, 000, '#'},
            {000, 000, 000, 000},
        }, {
            {000, 000, '#', '#'},
            {000, 000, '#', 000},
            {000, 000, '#', 000},
            {000, 000, 000, 000},
        }, {
            {000, '#', 000, 000},
            {000, '#', '#', '#'},
            {000, 000, 000, 000},
            {000, 000, 000, 000},
        }, {
            {000, 000, '#', 000},
            {000, 000, '#', 000},
            {000, '#', '#', 000},
            {000, 000, 000, 000},
        }
    }, {
        {
            {000, 000, 000, '#'},
            {000, '#', '#', '#'},
            {000, 000, 000, 000},
            {000, 000, 000, 000},
        }, {
            {000, '#', '#', 000},
            {000, 000, '#', 000},
            {000, 000, '#', 000},
            {000, 000, 000, 000},
        }, {
            {000, 000, 000, 000},
            {000, '#', '#', '#'},
            {000, '#', 000, 000},
            {000, 000, 000, 000},
        }, {
            {000, 000, '#', 000},
            {000, 000, '#', 000},
            {000, 000, '#', '#'},
            {000, 000, 000, 000},
        }
    }, {
        {
            {000, 000, 000, 000},
            {000, '#', '#', '#'},
            {000, 000, '#', 000},
            {000, 000, 000, 000},
        }, {
            {000, 000, '#', 000},
            {000, 000, '#', '#'},
            {000, 000, '#', 000},
            {000, 000, 000, 000},
        }, {
            {000, 000, '#', 000},
            {000, '#', '#', '#'},
            {000, 000, 000, 000},
            {000, 000, 000, 000},
        }, {
            {000, 000, '#', 000},
            {000, '#', '#', 000},
            {000, 000, '#', 000},
            {000, 000, 000, 000},
        }
    },
};

tFigurePattern::tFigurePattern(unsigned int figureNumber)
{
    if(figureNumber == 0) throw std::exception();
    if(figureNumber > getMaxFigureNumber()) throw std::exception();
    number = figureNumber - 1;
}

tPoint tFigurePattern::getMaxFigSize()
{
    static bool isInit = false;
    static tPoint maxSize = {0, 0};
    if(isInit) return maxSize;
    for(unsigned int figureNumber = 0; figureNumber < figures.size(); figureNumber++)
    {
        unsigned int maxY = figures[figureNumber].size();
        if(maxY > (unsigned int)maxSize.y) maxSize.y = maxY;
        for(unsigned int y = 0; y < maxY; y++)
        {
            unsigned int maxX = figures[figureNumber][y].size();
            if(maxX > (unsigned int)maxSize.x) maxSize.x = maxX;
        }
    }
    return maxSize;
}

unsigned int tFigurePattern::getMaxFigState()
{
    return figures[number].size();
}

void tFigurePattern::nextFigState()
{
    state = (state + 1) % getMaxFigState();
}

void tFigurePattern::prevFigState()
{
    state = (state == 0) ? getMaxFigState() - 1 : (state - 1);
}

unsigned int tFigurePattern::getMaxFigureNumber()
{
    return figures.size();
}

std::vector<std::vector<int> > tFigurePattern::getFigState()
{
    return figures[number][state]; ;
}

tPoint tFigurePattern::getFigureSize()
{
    tPoint answer = {(int)figures[number][state][0].size(), (int)figures[number][state].size()};
    return answer;
}
