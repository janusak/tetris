#include "system.hpp"
#include <ncurses.h>
#include <vector>
#include "viewer.hpp"

static void setColor(unsigned int color);

int baseX = 1;
int baseY = 1;

static void drawBorder(int sizeX, int sizeY)
{

    int startLine = -1;
    int startXcol = -1;
    char border[] = "##";
    start_color();
    init_pair(8, COLOR_MAGENTA, COLOR_CYAN);
    color_set(8, NULL);
    for(int y = -1; y <= sizeY; y++)
    {
        if(y == sizeY||y == startLine)
        {
            for(int x = startXcol; x <= sizeX; x++)
            {
                mvaddstr(baseY + y, (baseX + x) * 2, border);
            }
        }
        else
        {
            mvaddstr(baseY + y, (baseX + startXcol) * 2, border);
            mvaddstr(baseY + y, (baseX + sizeX) * 2, border);
        }
    }
}

void setColor(unsigned int color)
{
    start_color();
    init_pair(1, COLOR_WHITE, COLOR_RED);
    init_pair(2, COLOR_WHITE, COLOR_GREEN);
    init_pair(3, COLOR_WHITE, COLOR_YELLOW);
    init_pair(4, COLOR_WHITE, COLOR_BLUE);
    init_pair(5, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(6, COLOR_WHITE, COLOR_CYAN);
    init_pair(7, COLOR_WHITE, COLOR_GREY);
    color = color % 7 + 1;
    color_set(color, NULL);
}

void drawTiles(const tDrawData& drawData)
{
    const std::vector<std::vector<int> > & tiles = drawData.tiles;
    erase();
    drawBorder(tiles[0].size(), tiles.size());
    for(unsigned int y = 0; y < tiles.size(); y++)
    {
        for(unsigned int x = 0; x < tiles[y].size(); x++)
        {
            if(tiles[y][x] != 0)
            {
                setColor(tiles[y][x]);
                mvaddstr(baseY + y, (baseX + x) * 2, "[]");
                use_default_colors();
            }
        }
    }
    refresh();
}
