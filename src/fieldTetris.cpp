#include <exception>
#include "types.hpp"
#include "field.hpp"
#include "fieldTetris.hpp"
#include "figure.hpp"

static bool pointValidate(const tPoint& point);
std::vector<std::vector<int> > & moveOneLineDown(std::vector<std::vector<int> > & tiles, int line);
std::vector<std::vector<int> > initTiles(const tPoint size);

bool pointValidate(const tPoint& point)
{
    if(point.x < tFigurePattern::getMaxFigSize().x) return false;
    if(point.y < tFigurePattern::getMaxFigSize().y) return false;
    return true;
}

std::vector<std::vector<int> > & moveOneLineDown(std::vector<std::vector<int> > & tiles, int line)
{
    for(unsigned int y = line; y > 0; y--)
    {
        tiles[y] = tiles[y - 1];
    }
    tiles[0] = std::vector <int> (tiles[0].size());
    return tiles;
}

tFieldTetris::tFieldTetris(const tPoint& size):
    tField(size, tPoint(size.x / 2, tFigurePattern::getMaxFigSize().y / 2))
{
    if(pointValidate(size) == false) throw std::exception();
}

bool tFieldTetris::update()
{
    bool answer = false;
    for(unsigned int y = 0; y < tiles.size(); y++)
    {
        if(isLineFilled(y))
        {
            moveOneLineDown(tiles, y);
            answer = true;
        }
    }
    return answer;
}

bool tFieldTetris::isLineFilled(const int& line)
{
    for(unsigned int x = 0; x < tiles[line].size(); x++)
    {
        if(tiles[line][x] == 0) return false;
    }
    return true;
}
