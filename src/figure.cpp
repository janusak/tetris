#include <iostream>
#include "figure.hpp"

static tPoint getCenter(const tPoint&);

tFigure::tFigure(unsigned int figureNumber):
    figure(figureNumber),
    field(
        figure.getFigState(),
        tPoint(tFigurePattern::getMaxFigSize().x / 2, tFigurePattern::getMaxFigSize().y / 2)
    ),
    color(figureNumber)
{}

tPoint tFigure::getFigureSize()
{
    return field.getWidthAndHeight();
}

unsigned int tFigure::getMaxFigureNumber()
{
    return tFigurePattern::getMaxFigureNumber();
}

bool tFigure::tryMerge(const tField& fieldToMerge)
{
    tPoint startMerge = field.getLeftUpPoint();
    startMerge.x += position.x;
    startMerge.y += position.y;
    std::vector<std::vector<int> > tiles = field.getTiles();
    for(
        int y = startMerge.y, tileY = 0;
        tileY < 4;
        y++, tileY++)
    {
        for(
            int x = startMerge.x, tileX = 0;
            tileX < 4;
            x++, tileX++)
        {
            if(tiles[tileY][tileX] != 0)
            {
                tPoint point = {x, y};
                if(fieldToMerge.isFilled(point))
                {
                    return false;
                }
            }
        }
    }
    return true;
}

void tFigure::merge(tField& fieldToMerge)
{
    tPoint startMerge = field.getLeftUpPoint();
    startMerge.x += position.x;
    startMerge.y += position.y;
    std::vector<std::vector<int> > tiles = field.getTiles();
    for(
        int y = startMerge.y, tileY = 0;
        tileY < 4;
        y++, tileY++)
    {
        for(
            int x = startMerge.x, tileX = 0;
            tileX < 4;
            x++, tileX++)
        {
            if(tiles[tileY][tileX] != 0)
            {
                tPoint point = {x, y};
                if(fieldToMerge.isFilled(point))
                {
                    std::cout << "Ошибка поле заполнено!" << std::endl;
                    throw std::exception();
                }
                if(!fieldToMerge.setTile(point, color))
                {
                    std::cout << "Ошибка не удалось установить значение поля!" << std::endl;
                    throw std::exception();
                }
            }
        }
    }
}

tPoint getCenter(const tPoint& figSize)
{
    tPoint answer = {figSize.x / 2, figSize.y / 2};
    return answer;
}

void tFigure::updateField()
{
    field = tField(figure.getFigState(), getCenter(figure.getFigureSize()));
}

bool tFigure::turn(const tField& fieldToMerge)
{
    figure.nextFigState();
    updateField();
    if(tryMerge(fieldToMerge))
    {
        return true;
    }
    figure.prevFigState();
    updateField();
    return false;
}

bool tFigure::moveLeft(const tField& fieldToMerge)
{
    position.x--;
    if(tryMerge(fieldToMerge))
    {
        return true;
    }
    position.x++;
    return false;
}

bool tFigure::moveRight(const tField& fieldToMerge)
{
    position.x++;
    if(tryMerge(fieldToMerge))
    {
        return true;
    }
    position.x--;
    return false;
}

bool tFigure::moveDown(const tField& fieldToMerge)
{
    position.y++;
    if(tryMerge(fieldToMerge))
    {
        return true;
    }
    position.y--;
    return false;

}

bool tFigure::drop(const tField& fieldToMerge)
{
    bool ans = moveDown(fieldToMerge);
    while(moveDown(fieldToMerge));
    return ans;
}
int tFigure::getColor()
{
    return color;
}
