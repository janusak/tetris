#include <vector>
#include <exception>
#include <cstddef>
#include "types.hpp"
#include "field.hpp"

std::vector<std::vector<int> > initArrayTiles(const std::vector<std::vector<int> > &);
std::vector<std::vector<int> > initArrayTiles(const tPoint&);
bool sizeValidate(const tPoint&);

std::vector<std::vector<int> > initArrayTiles(const std::vector<std::vector<int> > & tiles)
{
    unsigned int maxX = 0;
    for(unsigned int y = 0; y < tiles.size(); y++)
    {
        unsigned int sizeX = tiles[y].size();
        if(sizeX > maxX) maxX = sizeX;
    }
    std::vector<std::vector<int> > answer = std::vector<std::vector<int> > (tiles.size());
    for(unsigned int y = 0; y < tiles.size(); y++)
    {
        unsigned int x = tiles[y].size();
        std::vector <int> line = std::vector <int> (maxX);
        while(x--)
        {
            line[x] = tiles[y][x];
        }
        answer[y] = line;
    }
    return answer;
}

std::vector<std::vector<int> > initArrayTiles(const tPoint& size)
{
    std::vector<std::vector<int> > answer = std::vector<std::vector<int> > ((unsigned int)size.y);
    for(unsigned int y = 0; y < (unsigned int)size.y; y++)
    {
        answer[y] = std::vector <int> ((unsigned int)size.x);
    }
    return answer;
}

tField::tField(const std::vector<std::vector<int> > & tilesValue, const tPoint& centerValue)
{
    tiles = initArrayTiles(tilesValue);
    center = centerValue;
}

bool sizeValidate(const tPoint& size)
{
    if(size.x <= 0) return false;
    if(size.y <= 0) return false;
    return true;
}

tField::tField(const tPoint& size, const tPoint& centerValue)
{
    if(sizeValidate(size) == false) throw std::exception();
    tiles = initArrayTiles(size);
    center = centerValue;
}

bool tField::isFilled(const tPoint& pointValue) const
{
    if(isPartOfField(pointValue))
    {
        tPoint point = getVectorIndexes(pointValue);
        if(tiles[point.y][point.x] != 0) return true;
        return false;
    }
    return true;
}

bool tField::isPartOfField(const tPoint& pointValue) const
{
    tPoint point = getVectorIndexes(pointValue);
    if((unsigned int)point.y >= tiles.size()) return false;
    if((unsigned int)point.x >= tiles[point.y].size()) return false;
    return true;
}

int tField::getTile(const tPoint& pointValue) const
{
    if(isPartOfField(pointValue))
    {
        tPoint point = getVectorIndexes(pointValue);
        return tiles[point.y][point.x];
    }
    return 0;
}

bool tField::setTile(const tPoint& pointValue, const int& value)
{
    if(isPartOfField(pointValue))
    {
        tPoint point = getVectorIndexes(pointValue);
        tiles[point.y][point.x] = value;
        return true;
    }
    return false;
}

tPoint tField::getLeftUpPoint() const
{
    tPoint answer = {-center.x, -center.y};
    return answer;
}

tPoint tField::getWidthAndHeight() const
{
    tPoint answer = {(int)tiles[0].size(), (int)tiles.size()};
    return answer;
}

tPoint tField::getCenter() const
{
    return center;
}

std::vector<std::vector<int> > tField::getTiles() const
{
    return tiles;
}

tPoint tField::getVectorIndexes(const tPoint &point) const
{
    tPoint answer = {point.x + center.x, point.y + center.y};
    return answer;
}
